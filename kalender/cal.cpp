#include "cal.h"


Weekday intToDay(int i)
{
	switch (i) {
	case 1:
		return Weekday::monday;
	case 2:
		return Weekday::tuesday;
	case 3:
		return Weekday::wednesday;
	case 4:
		return Weekday::thursdag;
	case 5:
		return Weekday::friday;
	case 6:
		return Weekday::saturday;
	case 7:
		return Weekday::sunday;
	}
}

map <int, vector<Day>> fill_calendar(int uketall, Weekday w) {
	map <int, vector<Day>> uker;
	for (int ukenr = 1; ukenr <= uketall; ukenr++) {
		int i = int(w);
		while (i <= 7) {
			w = intToDay(i);
			uker[ukenr].push_back(Day(w));
			i++;
		}
		w = Weekday::monday;
	}
	return uker;
}

void print_calendar(map<int, vector<Day>> m) {
	for (auto a : m) {
		cout << "Uke " << a.first << endl;
		for (auto b : a.second) {
			cout << b;
		}
		cout << endl;
	}
}



/*#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <ctime>
#include "cal.h"

using namespace std;



void print_time() {
	// current date/time based on current system
	time_t now = time(0);

	cout << "Number of sec since January 1,1970:" << now << endl;

	tm *ltm = localtime(&now);

	// print various components of tm structure.
	cout << "Year" << 1970 + ltm->tm_year << endl;
	cout << "Month: " << 1 + ltm->tm_mon << endl;
	cout << "Day: " << ltm->tm_mday << endl;
	cout << "Time: " << 1 + ltm->tm_hour << ":";
	cout << 1 + ltm->tm_min << ":";
	cout << 1 + ltm->tm_sec << endl;
}*/