#pragma once
#include <vector>
#include <string>
using namespace std;

class exercise {
private:
	string name;
	int sets;
	int reps;
	double weight;

	double inc_weight;
	int inc_rep;
	int switch_up; //Vi lager en vector med �velser denne byttes med. Det gjelder ogs� �velser som bytter fra 5x5 til 3x3 f.eks

	vector<exercise> alternate;
	
public:
	exercise();
	exercise(string n, int s, int r, double w, double i_w, int i_r, bool sw);
	exercise(string n, int s, int r, double w, double i_w, int i_r, bool sw, vector<exercise> a);
	int get_sets() { return sets; };
	int get_reps() { return reps; };
	double get_weight() { return weight; };
	string get_name() { return name; };
	double get_inc_weight() { return inc_weight; };
	int get_inc_rep() { return inc_rep; };
	int get_switch_up() { return switch_up; };
	vector<exercise> get_alt() { return alternate; };


	void set_sets(int s) { sets = s; };
	void set_reps(int r) { reps = r; };
	void set_weight(double w) { weight = w; };
	void inc() { weight += inc_weight; };
};
ostream& operator<<(ostream& os, exercise& e);

class session {
private:
	vector<exercise> exercises;
	int completed = 0;
	string path;

public:
	session();
	session(string path);
	vector<exercise> get_exercises() { return exercises; };
	void set_exercises(vector<exercise> e) {exercises = e; };
	int get_competed() { return completed; };
	int increase() { completed += 1; };
	string get_path() { return path; };
	void load_txt();
	void save_txt();

};
ostream& operator<<(ostream& os, session& s);

void lifting_interface();
void increment(vector<session>& s);