#pragma once
#include "Appointment.h"
#include <map>
#include <string>
#include <vector>
using namespace std;


enum class Weekday { monday = 1, tuesday, wednesday, thursdag, friday, saturday, sunday };
enum class Matrett {red_fish, white_fish, chicken, meat, veggie, null};

class Day {
private:
	bool lifting = false;
	bool cardio = false;
	Matrett mat = Matrett::null;
	Weekday weekday;
	map<int, vector<Appointment>> schedule;
	map<int, string> notes;
public:
	void set_lifting() { lifting = (!lifting); };
	bool get_lifting() { return lifting; };
	void set_cardio() { cardio = (!cardio); };
	bool get_cardio() { return cardio; };

	Matrett get_matrett() { return mat; };
	void set_matrett(Matrett m) { mat = m; };
	Weekday get_weekday() { return weekday; };
	
	void add_to_schedule(int hour, Appointment a, int duration);
	void add_note(string note);

	Day();
	Day(Weekday w);

	friend ostream& operator<<(ostream& os, Day& d);
};