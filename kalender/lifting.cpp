#include "lifting.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

exercise::exercise()
{
}

exercise::exercise(string n, int s, int r, double w, double i_w, int i_r, bool sw)
{
	name = n;
	sets = s;
	reps = r;
	weight = w;
	inc_weight = i_w;
	inc_rep = i_r;
	switch_up = sw;
}

exercise::exercise(string n, int s, int r, double w, double i_w, int i_r, bool sw, vector<exercise> a)
{
	name = n;
	sets = s;
	reps = r;
	weight = w;
	inc_weight = i_w;
	inc_rep = i_r;
	switch_up = sw;
	alternate = a;
}

session::session()
{
}

session::session(string path)
{

	//VELDIG MYE UN�DVENDIG KODE HER. PR�VER � LA LOAD-FUNKSJONEN V�RE EN RENERE VERSJON AV DENNE
	this->path = path;
	ifstream infile(path);
	string line;
	while(getline(infile, line)){
		string n = "";
		int s, r, i_r, sw;
		double w, i_w;
		vector<exercise> a;


		stringstream counter;
		counter << line;
		int words = 0;
		string buffer = "";
		while (!counter.eof())
		{
			counter >> buffer;
			words++;
		}
		//Word-counter for debug
		
		stringstream ss;
		ss << line;
		ss >> n >> s >> r >> w >> i_w >> i_r >> sw;
		for (int i = 1; i * 7 < words; i++) {
			string n = "";
			int s, r, i_r, sw;
			double w, i_w;
			ss >> n >> s >> r >> w >> i_w >> i_r >> sw;
			a.push_back(exercise(n, s, r, w, i_w/10, i_r, sw));
		}
		exercises.push_back(exercise(n, s, r, w, i_w/10, i_r, sw, a));
	}
	infile.close();
}

void session::load_txt()
{
	ifstream infile(this->path);
	string line;
	while (getline(infile, line, '\n')) {
		istringstream iss(line);
		string n;
		int s, r, i_r;
		double w, i_w;
		int sw;
		vector<exercise> a;
		iss >> n >> s >> r >> w >> i_w >> i_r >> sw;
		while (sw == 1) {
			string n = "";
			int s, r, i_r, sw;
			double w, i_w;
			iss >> n >> s >> r >> w >> i_w >> i_r >> sw;
			a.push_back(exercise(n, s, r, w, i_w / 10, i_r, sw));
		}
		this->exercises.push_back(exercise(n, s, r, w, i_w / 10, i_r, sw, a));
	}
	infile.close();
}


void session::save_txt()
{
	ofstream outfile(this->path);
	vector<exercise>::iterator it;
	vector<exercise> ex = this->get_exercises();
	ostringstream oss;
	for (it = ex.begin(); it < ex.end(); it++) {
		oss << it->get_name() << " " << it->get_sets() << " " << it->get_reps() << " " << it->get_weight() << " " << it->get_inc_weight()*10<< " " << it->get_inc_rep() << " " << it->get_switch_up() << " ";
		vector<exercise> alt = it->get_alt();
		if (it->get_switch_up() == 1) {
			for (int key = 0; key < alt.size(); key++)
			{
				oss << alt[key].get_name() << " " << alt[key].get_sets() << " " << alt[key].get_reps() << " " << alt[key].get_weight() << " " << alt[key].get_inc_weight()*10 << " " << alt[key].get_inc_rep() << " " << alt[key].get_switch_up() << " ";
			}
		}
		oss << '\n';
	}
	outfile << oss.str();
	outfile.close();
}

ostream & operator<<(ostream & os, exercise & e)
{
	os << e.get_name() << ": "<< e.get_reps() << " repetitions for " << e.get_sets() << " sets at " << e.get_weight() << " kg.\n";
	return os;
}

ostream & operator<<(ostream & os, session & s)
{
	cout << "Todays session: \n";
	vector<exercise>::iterator it;
	vector<exercise> ex = s.get_exercises();
	for (it = ex.begin(); it < ex.end(); it++) {
		os << *it;
	}
	return os;
}

void lifting_interface()
{
	vector<session> s;
	string path;
	cout << "name of session txt file\n";
	cin >> path;

	int input = 0;
	while (input != -1) {
		ifstream infile(path);
		string line;
		while (getline(infile, line)) {
			s.push_back(session(line));
		}
		infile.close();
		cout << "sessions loaded \n";

		cout << "Choose your action! '-1' to quit\n"
			<< "1: Print current sessions\n"
			<< "2: Rotate sessions by 1\n"
			<< "3: Rotate sessions by 1 and increment\n"
			<< "4: Edit an exercise\n"
			<< "5: Save changes\n";

		cin >> input;
		switch (input) {
		case 1:
			for (auto e : s) {
				cout << e << '\n';
			};
			break;
		case 2:
		{
			session first = s.front();
			for (unsigned int i = 0; i < s.size()-1; i++) {
				s[i] = s[i + 1];
			}
			s[s.size() - 1] = first;
		}
			break;
		case 3:
		{
			increment(s);
			session first = s.front();
			for (unsigned int i = 0; i < s.size() - 1; i++) {
				s[i] = s[i + 1];
			}
			s[s.size() - 1] = first;
		}
			break;
		case 4:
		{
			cout << "Which session? " << "(0 - " << s.size() - 1 << '\n';
			int ses;
			cin >> ses;
			vector<exercise> e = s[ses].get_exercises();
			cout << s[ses] << '\n' << "Hvilken �velse?" << "(0 - " << e.size() - 1 << '\n';
			int ex;
			cin >> ex;
			cout << "Definer hvordan denne �velsen skal v�re p� formen 'navn, sets, reps, vekt, increment_vekt, increment_reps, alternative �velser(1/0)\n";
			cout << "DENNE ER UFERDIG\n";
		}
			break;
		}
		ofstream outfile(path);
		for (auto d : s) {
			outfile << d.get_path() << '\n';
		}
		outfile.close();
		s.clear();
	}
}

void increment(vector<session>& s) //kr�sjer
{
	vector<exercise> e_done = s[0].get_exercises();
	for (auto exercise_done : e_done) {
		for (auto session : s) {
			vector<exercise> e = session.get_exercises();
			for (auto exercise : e) {
				if (exercise.get_name() == exercise_done.get_name()) {
					exercise.inc();
				}
			}
			session.set_exercises(e);
			session.save_txt();
		}
	}
	for (auto d : s) {
		d.load_txt();
	}
}
