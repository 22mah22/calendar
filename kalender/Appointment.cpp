#include "Appointment.h"
#include <string>
#include <iostream>
using namespace std;


Appointment::Appointment(string e, string p)
{
	this->action = e;
	this->place = p;
}

Appointment::Appointment()
{
}


ostream& operator<<(ostream & os, const Appointment & a)
{
	if (a.action != "" && a.place != "") {
		os << a.action << " at " << a.place;
	}
	
	return os;
}
