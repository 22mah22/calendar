#pragma once
#include <string>
#include <iostream>
using namespace std;

class Appointment {
private:
	string action;
	string place;
public:
	const string get_event() { return action; };
	const string get_place() { return place; };
	Appointment(string e, string p);
	Appointment();
	friend ostream& operator<<(ostream & os, const Appointment& a);
};


