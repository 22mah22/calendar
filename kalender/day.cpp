#include "day.h"
#include "Appointment.h"

void Day::add_to_schedule(int hour, Appointment a, int duration)
{
	for (int i = 0; i<duration; i++){
		schedule[hour + i].push_back(a);
	}
	
}

void Day::add_note(string note)
{
	notes[notes.size()] = note;
}

Day::Day()
{

	Appointment emptyApp;
	for (int i = 8; i < 24; i++) {
		schedule[i].push_back(emptyApp);
	}
}

Day::Day(Weekday w) : Day()
{
	this->weekday = w;
	switch (w)
	{
	case Weekday::monday: 
		this->set_cardio();
		mat = Matrett::chicken;
		break;
	case Weekday::tuesday:
		this->set_lifting();
		mat = Matrett::red_fish;
		break;
	case Weekday::wednesday:
		this->set_cardio();
		mat = Matrett::chicken;
		break;
	case Weekday::thursdag:
		this->set_lifting();
		mat = Matrett::white_fish;
		break;
	case Weekday::friday:
		this->set_cardio();
		break;
	case Weekday::saturday: // code to be executed if n = 2;
		break;
	case Weekday::sunday:
		this->set_lifting();
		break;
	default: 
		cout << "Defaultmelding dag? \n";
	}
}

ostream & operator<<(ostream & os, Day & d)
{
	for (auto a : d.schedule) {
		cout << "Kl. " << a.first << " ";
		for (auto b : a.second) {
				os << b;
		}
		cout << '\n';
	}
	if (d.lifting) {
		os << "Lifting today: \n";
	}
	if (d.cardio) {
		os << "Cardio today: \n";
	}
	os << "Todays dinner: " << "TO BE IMPLEMENTED" << '\n';
	for (auto a : d.notes) {
		os << a.second << '\n';
	}
	return os;
}
